#include "server.hpp"
#include <connection/key_exchange.hpp>
#include <boost/endian/conversion.hpp>
#include <connection/forward.hpp>

using namespace boost;
using namespace endian;

namespace mvpn
{

server::session::session(server::session_attribute attr) :
  cfg_(attr.cfg),
  net_(std::move(attr.net)),
  tun_(attr.tun)
{}

server::shared_session server::make_session(server::session_attribute attr)
{
  return server::shared_session(new server::session(std::move(attr)));
}

void server::session::start()
{
  service::key_exchange::attribute attr;
  attr.peerKeys = cfg_.clients;
  attr.identify = cfg_.identify;
  attr.privateKey = cfg_.privateKey;
  auto key_exchange = service::make_key_exchange(
    std::move(net_.io),
    std::move(attr),
    service::key_exchange::type::SERVER,
    1000);
  key_exchange->async_exchange(
    SHARE_THIS(
      service::key_exchange::error_code ec,
      service::key_exchange::peer_attribute pAttr,
      ip::tcp::socket ps
    )
    {
      net_.io = std::move(ps);
      if ( ec.code != service::key_exchange::error_value::ERRSUCCESS )
      {
        LOG(ERROR) << ec.what;
        return;
      }
      key_ = pAttr.key;
      auto fwd = service::make_forward(tun_.io, std::move(net_.io), pAttr.key);
      fwd->start(
        [this](service::forward::error_code ec, boost::asio::ip::tcp::socket ps)
        {
          (void) this;
          LOG(ERROR) << ec.code.message();
        });
    }
  );
}

}
