//
// Created by maxtorm on 18-10-26.
//

#ifndef MVPN_CONNECTION_DEF_HPP
#define MVPN_CONNECTION_DEF_HPP

#include <boost/property_tree/ptree.hpp>
#include <boost/asio/io_context.hpp>
#include <boost/asio/ip/address.hpp>
#include <memory>

namespace mvpn::connection
{
using shared_ctx = std::shared_ptr<boost::asio::io_context>;
constexpr std::size_t KEY_LEN = 32;
constexpr std::size_t NONCE_LEN = 32;
}
#endif //MVPN_DEF_HPP
