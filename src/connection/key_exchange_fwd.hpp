//
// Created by maxtorm on 18-11-6.
//

#ifndef MVPN_KEY_EXCHANGE_FWD_HPP
#define MVPN_KEY_EXCHANGE_FWD_HPP
#include <memory>
namespace mvpn::service
{
class key_exchange;
using shared_key_exchange = std::shared_ptr<key_exchange>;
}
#endif //MVPN_KEY_EXCHANGE_FWD_HPP
