//
// Created by maxtorm on 18-11-7.
//

#ifndef MVPN_FORWARD_FWD_HPP
#define MVPN_FORWARD_FWD_HPP

#include <memory>
namespace mvpn::service
{
class forward;
using shared_forward = std::shared_ptr<forward>;

}
#endif //MVPN_FORWARD_FWD_HPP
