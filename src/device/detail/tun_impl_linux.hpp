// Created by maxtorm on 18-9-15.
//

#ifndef MVPN_DEVICE_DETAIL_TUN_IMPL_LINUX_H
#define MVPN_DEVICE_DETAIL_TUN_IMPL_LINUX_H

#include <boost/asio/posix/stream_descriptor.hpp>
#include <boost/asio/ip/address.hpp>
#include <boost/asio/ip/network_v4.hpp>
#include <boost/asio/ip/network_v6.hpp>
#include <boost/noncopyable.hpp>
#include <linux/if_tun.h>
#include <sys/socket.h>
#include <device/config.hpp>
#include <util/tool.hpp>
#include <glog/logging.h>

namespace mvpn::device::detail
{

class tun_impl_linux : boost::noncopyable
{
private:
  const char *TUN_PATH = "/dev/net/tun";
  boost::asio::posix::stream_descriptor fd_;
  int tun_fd_ = -1;
public:
  explicit tun_impl_linux(boost::asio::io_context &ctx) :
    fd_(ctx)
  {
  };
  
  ~tun_impl_linux()
  {
    close();
  }
  
  void open_tun()
  {
    tun_fd_ = ::open(TUN_PATH, O_RDWR);
    if ( tun_fd_ == -1 )
    {
      LOG(ERROR) << "unable to open " << TUN_PATH << ": "
                 << google::StrError(errno);
      exit(EXIT_FAILURE);
    }
  };
  
  void set_tun(const config &c)
  {
    auto if_req = make_ifreq(c.device);
    if_req.ifr_flags = IFF_NO_PI | IFF_TUN;
    ioctl(tun_fd_, TUNSETIFF, if_req, "ioctl(TUNSETIFF)");
  }
  
  void open(const config &config)
  {
    open_tun();
    set_tun(config);
    auto address = boost::asio::ip::make_address(config.ip);
    auto network_str = config.ip + "/" + config.prefix;
    if ( address.is_v4())
    {
      LOG(INFO) << "set up a ipv4 address ... " << address.to_string();
      int s = make_v4_sock();
      auto sock_guard = util::make_guard(s, &::close);
      auto network_v4 = boost::asio::ip::make_network_v4(network_str);
      s = make_v4_sock();
      set_network_v4(config.device, s, network_v4);
    }
    else
    {
      LOG(INFO) << "set up a ipv6 address ... " << address.to_string();
      int s = make_v4_sock();
      auto sock_guard = util::make_guard(s, &::close);
      auto network_v6 = boost::asio::ip::make_network_v6(network_str);
      s = make_v6_sock();
      set_network_v6(config.device, s, network_v6);
    }
    int s = make_v4_sock();
    auto sock_guard = util::make_guard(s, &::close);
    LOG(INFO) << "set up mtu ... " << config.mtu;
    set_mtu(config.device, s, config.mtu);
    LOG(INFO) << "device turn up ...";
    turn_up(config.device, s);
    fd_.assign(tun_fd_);
  }
  
  void close()
  {
    fd_.close();
  };
  
  template<typename MutableBufferSequence, typename ReadHandler>
  BOOST_ASIO_INITFN_RESULT_TYPE(
    ReadHandler,
    void (boost::system::error_code, std::size_t))
  async_read_some(const MutableBufferSequence &buffers,
                  BOOST_ASIO_MOVE_ARG(ReadHandler)handler)
  {
    BOOST_ASSERT(fd_.is_open());
    return fd_.async_read_some(
      buffers,
      std::forward<ReadHandler>(handler));
  }
  
  
  template<typename ConstBufferSequence, typename WriteHandler>
  BOOST_ASIO_INITFN_RESULT_TYPE(
    WriteHandler,
    void (boost::system::error_code, std::size_t))
  async_write_some(
    const ConstBufferSequence &buffers,
    BOOST_ASIO_MOVE_ARG(WriteHandler)handler)
  {
    BOOST_ASSERT(fd_.is_open());
    return fd_.async_write_some(
      buffers,
      std::forward<WriteHandler>(handler));
  }

private:
  
  inline ifreq make_ifreq(const std::string &device) const noexcept
  {
    auto if_req = util::make<ifreq>();
    strncpy(if_req.ifr_name, device.c_str(), IFNAMSIZ);
    return if_req;
  }
  
  template<typename T>
  inline void
  ioctl(int fd, unsigned int req, T &inout, std::string_view err_msg) noexcept
  {
    auto ret = ::ioctl(fd, req, reinterpret_cast<void *>(&inout));
    if ( ret == -1 )
    {
      LOG(ERROR) << err_msg << ": " << google::StrError(errno);
      exit(-1);
    }
  }
  
  inline int make_v4_sock()
  {
    int sock = socket(AF_INET, SOCK_DGRAM, 0);
    if ( sock == -1 )
    {
      LOG(ERROR) << "make ipv4 socket: " << google::StrError(errno);
      exit(-1);
    }
    return sock;
  }
  
  inline int make_v6_sock()
  {
    int sock = socket(AF_INET6, SOCK_DGRAM, 0);
    if ( sock == -1 )
    {
      LOG(ERROR) << "make ipv6 socket: " << google::StrError(errno);
      exit(-1);
    }
    return sock;
  }
  
  void set_mtu(const std::string &device, int sock, int mtu)
  {
    auto if_req = make_ifreq(device);
    if_req.ifr_mtu = mtu;
    ioctl(sock, SIOCSIFMTU, if_req, "ioctl(SIOCSIFMTU)");
  }
  
  void set_network_v4(
    const std::string &device,
    int sock,
    const boost::asio::ip::network_v4 &v4)
  {
    auto if_req = make_ifreq(device);
    auto address_bytes = v4.address().to_bytes();
    auto mask_bytes = v4.netmask().to_bytes();
    auto sai = util::make<sockaddr_in>();
    sai.sin_family = AF_INET;
    memcpy(&sai.sin_addr, address_bytes.data(), address_bytes.size());
    memcpy(&if_req.ifr_addr, &sai, sizeof(sai));
    ioctl(sock, SIOCSIFADDR, if_req, "ioctl(SIOCSIFADDR)");
    memcpy(&sai.sin_addr, mask_bytes.data(), mask_bytes.size());
    memcpy(&if_req.ifr_addr, &sai, sizeof(sai));
    ioctl(sock, SIOCSIFNETMASK, if_req, "ioctl(SIOCSIFNETMASK)");
  }
  
  void set_network_v6(const std::string &device, int sock6,
                      const boost::asio::ip::network_v6 &v6)
  {
    auto if_req = make_ifreq(device);
    struct
    {
      in6_addr addr;
      uint32_t prefix;
      unsigned int index;
    } in6_ifreq{};
    util::clean(in6_ifreq);
    auto address_bytes = v6.address().to_bytes();
    memcpy(&in6_ifreq.addr, address_bytes.data(), address_bytes.size());
    in6_ifreq.prefix = v6.prefix_length();
    ioctl(sock6, SIOCGIFINDEX, if_req, "ioctl(SIOCGIFINDEX)");
    in6_ifreq.index = (unsigned int) if_req.ifr_ifindex;
    ioctl(sock6, SIOCSIFADDR, in6_ifreq, "ioctl(SIOCSIFADDR)");
  }
  
  void turn_up(const std::string &device, int sock)
  {
    auto if_req = make_ifreq(device);
    ioctl(sock, SIOCGIFFLAGS, if_req, "ioctl(SIOCGIFFLAGS)");
    if_req.ifr_flags |= IFF_UP | IFF_RUNNING;
    ioctl(sock, SIOCSIFFLAGS, if_req, "ioctl(SIOCSIFFLAGS)");
  }
};

}
#endif
