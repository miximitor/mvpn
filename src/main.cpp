#include <boost/asio.hpp>
#include <connection/key_exchange.hpp>
#include <cryptopp/base64.h>
#include <cryptopp/filters.h>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <iostream>
#include <device/tun.hpp>
#include <client/client.hpp>
#include <server/server.hpp>
using namespace boost;
using namespace asio;
using namespace CryptoPP;
using namespace mvpn;
property_tree::ptree pt;
void cli()
{
  io_context ctx;
  device::tun tun(ctx);
  device::config tun_cfg(pt.get_child("Interface"));
  tun.open(tun_cfg);
  client::config cli_cfg(pt.get_child("Client"));
  client c(cli_cfg,ctx,tun);
  c.start();
  ctx.run();
}

void serv()
{
  io_context ctx;
  device::tun tun(ctx);
  device::config tun_cfg(pt.get_child("Interface"));
  tun.open(tun_cfg);
  server::config serv_cfg(pt.get_child("Server"));
  server s(serv_cfg,ctx,tun);
  s.start();
  ctx.run();
}

int main(int argc, char *argv[])
{
  google::InitGoogleLogging(argv[0]);
  google::SetStderrLogging(google::GLOG_WARNING);
  property_tree::json_parser::read_json("mvpn.json",pt);
  if (pt.get<std::string>("Type") == "Server")
  {
    LOG(INFO) << "running as server";
    serv();
  }
  else
  {
    LOG(INFO) << "running as client";
    cli();
  }
}

